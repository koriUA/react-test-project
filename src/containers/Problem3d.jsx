import React from "react";
import { withRouter } from 'react-router-dom';
import { AccountDetailsWithLoading } from '../components/AccountDetailsWithLoading'



class Problem3d extends React.Component {

  render() {
    const user = {id: 1};
    return (
      <div>
        <h1>Problem 3 (d)</h1>
        <AccountDetailsWithLoading user={user}></AccountDetailsWithLoading>
      </div>
    );
  }
}


export default withRouter(Problem3d);
