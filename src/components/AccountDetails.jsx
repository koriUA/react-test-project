import React from "react";

export default function AccountDetails({account}) {
    return (
        <div>
            <div>{account.id}</div>
            <div>{account.name}</div>
            <div>{account.status}</div>
        </div>
    )
}
