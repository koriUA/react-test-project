import React from "react";
import Loading from "./Loading";
import AccountDetails from "./AccountDetails";
import {getAccountInfo} from "../utils/get-account-info";

function withLoading(ChildComponent, fetchData) {
    return class extends React.Component {
        constructor() {
            super();
            this.state = {
                loading: true
            }
        }
        componentDidMount() {
            fetchData(this.props.user).then((accountInfo) => {
                this.setState(() => ({
                    account: accountInfo,
                    loading: false
                }));
            });
        }
        render() {
            return (
                <div>
                    {this.state.loading ?
                        <Loading message="Loading....." /> :
                        <ChildComponent {...this.props} {...this.state}></ChildComponent>}
                </div>
            )
        }
    };
}

export const AccountDetailsWithLoading = withLoading(AccountDetails, getAccountInfo);