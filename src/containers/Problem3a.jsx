import React from "react";
import {withRouter} from 'react-router-dom';
import Loading from '../components/Loading';
import AccountDetails from '../components/AccountDetails';
import Account from '../components/Account';


class Problem3a extends React.Component {

    render() {
        const user = {id: 1};
        return (
            <div>
                <h1>Problem 3 (a)</h1>
                <Account user={user}>
                    {(accountInfo) => accountInfo == null ?
                        <Loading message="Loading..."/> :
                        <AccountDetails account={accountInfo}/>}
                </Account>

            </div>
        );
    }
}


export default withRouter(Problem3a);
