import React from "react";
import { withRouter } from 'react-router-dom';
import AccountSearch from '../components/AccountSearch';

class Problem3c extends React.Component {

  accountSearchHandler = (value) => {
    console.log('value = ', value);
  };

  render() {
    return (
        <div>
          <h1>Problem 3 (c)</h1>
          <AccountSearch />
        </div>
    );
  }
}


export default withRouter(Problem3c);
