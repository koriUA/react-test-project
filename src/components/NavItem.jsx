import React from "react";
import {Link, withRouter} from "react-router-dom";

class NavItem extends React.Component {

  render() {
    const isActive = this.props.location.pathname === this.props.to;
    const { staticContext, ...rest } = this.props;
    return (
      <li className={isActive ? 'nav-item active' : 'nav-item'}>
        <Link className={'nav-link'} {...rest}></Link>
      </li>
    )
  }
}

export default withRouter(NavItem);
