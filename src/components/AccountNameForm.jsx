import React from "react";

const formStyles = {'width': '200px', 'margin': '0 auto'};
export default class AccountNameForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: ''
        }
    }

    handleInputChange = ({target}) => {
        const value = target.value;
        const name = target.name;
        this.setState(() => ({
            [name]: value
        }));
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.state.name);
    };

    render() {
        return (
            <form action="" style={formStyles} onSubmit={this.handleSubmit}>
                <div className="form-group text-left">
                    <label htmlFor="name">Name:</label>
                    <input
                        id="name"
                        name="name"
                        type="text"
                        placeholder="name"
                        className="form-control mb-1"
                        value={this.state.name}
                        onChange={this.handleInputChange}
                    />
                    <input className="btn btn-secondary" type="submit" value="Submit"/>
                </div>
            </form>
        );
    }
}