import React from 'react';
import './App.css';
import {HashRouter as Router, Route, Redirect, Switch} from "react-router-dom";
import 'bootstrap-css-only/css/bootstrap.min.css';

import Header from "./components/Header";

import Problem3a from "./containers/Problem3a";
import Problem3b from "./containers/Problem3b";
import Problem3c from "./containers/Problem3c";
import Problem3d from "./containers/Problem3d";

function App() {
  return (
    <div className="App">
        <Router>
            <Header/>
            <div className="container-fluid">
                <Switch>
                    <Route path="/problem3-a" component={Problem3a}></Route>
                    <Route path="/problem3-b" component={Problem3b}></Route>
                    <Route path="/problem3-c" component={Problem3c}></Route>
                    <Route path="/problem3-d" component={Problem3d}></Route>
                    <Redirect to="/problem3-a"></Redirect>
                </Switch>
            </div>
        </Router>
    </div>
  );
}

export default App;
