
/**
 * Assume getAccountInfo has the following signature:
 * const getAccountInfo = (user) => {
 *   return new Promise((resolve) => {
 *     fetch(`/api/accountInfo/${user.id}`).then((accountInfo) => resolve(accountInfo.json()));
 *   });
 * };
 *
 * and the returned accountInfo object is: { name: string, id: number, status: string }
 */
export const getAccountInfo = (user) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve({name: 'John', id: 22, status: 10})
        }, 1000);
    })
};
