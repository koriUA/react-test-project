import React from "react";
import {getAccountInfo} from "../utils/get-account-info";

export default class Account extends React.Component {

    constructor() {
        super();
        this.state = {}
    }

    fetchData() {
        this.setState(() => ({accountInfo: null}));
        getAccountInfo(this.props.user).then((accountInfo) => {
            this.setState(() => ({accountInfo}));
        });
    }

    componentDidUpdate(prevProps, nextProps) {
        if (this.props.user.name !== prevProps.user.name) {
            this.fetchData();
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        return (<div>{this.props.children(this.state.accountInfo)}</div>)
    }
}