import React from "react";
import NavItem from './NavItem';

export default class Nav extends React.Component {
    constructor() {
        super();
        this.state = {
            collapsed: true,
        };
    }

    toggleCollapse = () => {
        const collapsed = !this.state.collapsed;
        this.setState({collapsed});
    }

    render() {
        const {collapsed} = this.state;
        const navClass = collapsed ? "collapse" : "";
        return (
            <nav className="navbar navbar-expand-lg navbar-toggleable-md navbar-light bg-light" role="navigation">
                <div className="container">
                    <button className="navbar-toggler navbar-toggler-right" type="button" onClick={this.toggleCollapse}>
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <a className="navbar-brand" href="#">Test React App</a>
                    <div className={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
                        <ul className="navbar-nav mr-auto">
                            <NavItem to="/problem3-a">Problem 3 (a)</NavItem>
                            <NavItem to="/problem3-b">Problem 3 (b)</NavItem>
                            <NavItem to="/problem3-c">Problem 3 (c)</NavItem>
                            <NavItem to="/problem3-d">Problem 3 (d)</NavItem>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}
