import React from "react";
import {withRouter} from 'react-router-dom';
import AccountNameForm from '../components/AccountNameForm';

class Problem3b extends React.Component {

    accountSearchHandler = (value) => {
        console.log('accountSearchHandler', value);
    };

    render() {
        return (
            <div>
                <h1>Problem 3 (b)</h1>
                <AccountNameForm onSubmit={this.accountSearchHandler}/>
            </div>
        );
    }
}


export default withRouter(Problem3b);
