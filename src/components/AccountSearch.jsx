import React from "react";
import AccountNameForm from "./AccountNameForm";
import Account from "./Account";
import Loading from "./Loading";
import AccountDetails from "./AccountDetails";

export default class AccountSearch extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {id: 11, name: ''}
        };
    }

    accountSearchHandler = (value) => {
        this.setState(() => {
            return ({
                user: {id: 11, name: value}
            });
        })
    };

    render() {
        return (
            <div>
                <h4>Account Search</h4>
                <AccountNameForm onSubmit={this.accountSearchHandler} />
                <hr/>
                <Account user={this.state.user}>
                    {(accountInfo) => accountInfo == null ?
                        <Loading message="Loading..."/> :
                        <AccountDetails account={accountInfo}/>}
                </Account>
            </div>
        );
    }
}